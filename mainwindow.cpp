#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->widget->xAxis->setRange(0, 100);
    ui->widget->yAxis->setRange(0, 100);

    tmr = new QTimer(this);
    tmr->setInterval(10); // Задаем интервал таймера
    connect(tmr, SIGNAL(timeout()), this, SLOT(updateTime()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
char in_arr[10000];
char old_y[100];

int x_axis = 0;

void MainWindow::updateTime()
{
    QVector<double> x(100), y(100);

    qint64 bytes_readed = file.read(in_arr, 100);
    if(bytes_readed <= 0)
        return;

    ui->label_7->setText(QString::number(file.pos()));

    for (int i = 0; i < bytes_readed; i++)
    {
      old_y[x_axis] = in_arr[i];

      x_axis++;
      if(x_axis > 99) {
          x_axis = 0;
      }
    }

    for (int i = 0; i < 100; i++)
    {
      x[i] = i;
      y[i] = old_y[i];
    }

    if(bytes_readed > 0) {
        ui->widget->addGraph();
        ui->widget->graph(0)->setData(x, y);
        ui->widget->replot();
    }
}

void MainWindow::on_pushButton_clicked()
{
    open_filename = QFileDialog::getOpenFileName(this, "Open File", "C://", "Bin files (*.*)");
    ui->lineEdit->setText(open_filename);
}

void MainWindow::on_pushButton_2_clicked()
{
    tmr->start();

    if(file.isOpen() == false) {
        file.setFileName(open_filename);
        file.open(QIODevice::ReadOnly);
    }
}

void MainWindow::on_comboBox_2_activated(const QString &arg1)
{
    ui->widget->xAxis->setRange(0, 100);
    ui->widget->yAxis->setRange(0, 100);
    ui->widget->graph(0)->rescaleAxes();
}
