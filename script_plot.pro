#-------------------------------------------------
#
# Project created by QtCreator 2018-07-04T17:31:57
#
#-------------------------------------------------

QT       += core gui
QT       += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = script_plot
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot/qcustomplot.cpp \
    crc16.cpp

HEADERS  += mainwindow.h \
    qcustomplot/qcustomplot.h \
    crc16.h

FORMS    += mainwindow.ui
